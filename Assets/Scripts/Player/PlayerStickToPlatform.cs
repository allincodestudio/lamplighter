using System.Collections;
using System.Collections.Generic;
using StateMachine;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerStickToPlatform : MonoBehaviour
{
    private MovePlatformRightAndLeft _movePlatformRightAndLeft;
    private MovePlatformUpAndDown _movePlatformUpAndDown;

    private CharacterCtr _characterCtr;

    private GameObject _platformGameObject;

    private Vector2 _xyPlayerIdleOffset;
    private Vector2 _xyPlayerWalkOffset;

    private bool _onPlatform = false;

    private PlatformCheck _platformCheck;

    private Rigidbody2D _rigidbody2D;

    private void Start()
    {
        _characterCtr = GetComponent<CharacterCtr>();
        _platformCheck = GetComponentInChildren<PlatformCheck>();
        _rigidbody2D = GetComponent<Rigidbody2D>();

    }

     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
        {

            this.transform.SetParent(other.transform);
            
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
        {
            this.transform.SetParent(null);
        }
    }

    /*
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag.Equals("Platform"))
        {
           Debug.Log("Collsision");
            _platformGameObject = other.gameObject;
            _movePlatformRightAndLeft = other.gameObject.GetComponent<MovePlatformRightAndLeft>();
            _movePlatformUpAndDown = other.gameObject.GetComponent<MovePlatformUpAndDown>();
            //_onPlatform = true;
            _xyPlayerIdleOffset = transform.position - other.transform.position;
            //_xyPlayerWalkOffset = _xyPlayerIdleOffset;
            this.transform.parent = other.transform;
            _onPlatform = true;
            //this.transform.SetParent(other.transform);

            

        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag.Equals("Platform"))
        {
           // _onPlatform = false;
            
        }
    }

    private void Update()
    {
        //if(_platformGameObject != null)
        //_xyPlayerWalkOffset = transform.position - _platformGameObject.transform.position;

        if(!_platformCheck.CheckCollision() && _onPlatform){
            _platformGameObject = null;
            
            this.transform.parent = null;
        _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        _onPlatform = false;
        return;
        }

        if(_platformCheck.CheckCollision() && _platformGameObject != null){

        
         _rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
         Debug.Log("platform");
        
        //this.transform.parent = other.transform;


        
        if (!_onPlatform)
            return;

        Debug.Log(_characterCtr.activeState.GetType().Name);
        if (_characterCtr.activeState.GetType().Name == "IdleState")
        {
            transform.position = new Vector3(_platformGameObject.transform.position.x + _xyPlayerIdleOffset.x, _platformGameObject.transform.position.y + _xyPlayerIdleOffset.y, transform.position.z);
            return;
        }

        if (_characterCtr.activeState.GetType().Name == "WalkState")
        {
            transform.position = new Vector3(transform.position.x, _platformGameObject.transform.position.y + _xyPlayerIdleOffset.y, transform.position.z);
            _xyPlayerIdleOffset.x = transform.position.x - _platformGameObject.transform.position.x;
        
        }
        }
    }
   

    */

     private void FixedUpdate()
    {

        
        
       Debug.Log( _characterCtr.activeState.GetType().Name);
    }

  

    public float PlatformMoveSpeedReturn()
    {

        return _movePlatformRightAndLeft.moveSpeed;
    }

    public bool IsPlayerOnPlatform()
    {
        return _onPlatform;
    }

}
