using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "States/Character/Fall")]
    public class FallState : State<CharacterCtr>
    {
        private GroundCheck _groundCheck;

        [SerializeField] private float _maxFallSpeed = -10f;

        private Rigidbody2D _rigidbody2D;

        private SwitchSpriteSide _switchSpriteSide;

        private Animator _animator;

        private LadderCheck _ladderCheck;

        private Vector2 _inputXY;

        private TestPlayerMove _testPlayerMove;

        private CinemachineImpulseSource _cinemachineImpulseSource;

        private PlatformCheck _platformCheck;

        private float _fallTime = 0f;

        public override void Init(CharacterCtr parent)
        {
            base.Init(parent);
            if (_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if (_animator == null) _animator = parent.animator;
            if (_groundCheck == null) _groundCheck = parent.groundCheck;
            if (_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            if (_switchSpriteSide == null) _switchSpriteSide = parent._switchSpriteSide;
            if (_cinemachineImpulseSource == null) _cinemachineImpulseSource = parent.cinemachineImpulseSource;
            if(_platformCheck == null) _platformCheck = parent.platformCheck;


        }

        public override void AnimationStart()
        {
            if (!_animator.GetBool("PlayerFall")) _animator.SetBool("PlayerFall", true);
        }
        public override void CaptureInput()
        {
            _inputXY = _testPlayerMove.moveVector;
        }

        public override void ChangeState()
        {
            if (_groundCheck.CheckCollision() | _platformCheck.CheckCollision() && _rigidbody2D.velocity.x != 0) _runner.SetState(typeof(WalkState));
            if (_groundCheck.CheckCollision() | _platformCheck.CheckCollision() && _rigidbody2D.velocity == Vector2.zero) _runner.SetState(typeof(IdleState));
            if (_ladderCheck.LadderColideCheck() && _inputXY.y != 0) _runner.SetState(typeof(LadderClimbState));
        }

        public override void Exit()
        {
            //if (_fallTime > 0.5f)
             //   _cinemachineImpulseSource.GenerateImpulse();

            _animator.SetBool("PlayerFall", false);
            _fallTime = 0f;
        }

        public override void FixedUpdate()
        {
            _switchSpriteSide.SwitchSpriteSideX(_inputXY.x);
            _fallTime += Time.deltaTime;
            if (_rigidbody2D.velocity.y < _maxFallSpeed)
            {
                _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _maxFallSpeed);
            }
        }

        public override void Update()
        {

        }
    }
}
