using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private InputReader inputReader;
    [SerializeField] private int playerMoveSpeed = 5;
    [SerializeField] private int jumpForce = 5;

    private PlayerStates playerStates;

    private Vector2 moveImput;

    private Vector2 playerVelocity = Vector2.zero;

    private Rigidbody2D rigidBody2D;

    private Transform transformObject;

    private Vector3 lookVector3 = Vector3.zero;

    private Animator animator;

    private CapsuleCollider2D playerCollider2D;

    private bool isPlayerOnGround = false;

    private bool isJumping = false;

    //private bool canClimb = false;
    private float baseGravityScale;

    private bool canPlayerMove = false;
    private Vector2 newMovevector = Vector2.zero;
    private float acceleration = 2f;

    private PlayerStates currentPlayerState;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.GameStartEvent += PlayerCanMove;
        GameManager.GameStopEvent += PlayerCantMove;
        animator = GetComponent<Animator>();
        rigidBody2D = GetComponent<Rigidbody2D>();
        transformObject = GetComponent<Transform>();
        playerCollider2D = GetComponent<CapsuleCollider2D>();
        baseGravityScale = rigidBody2D.gravityScale;
        inputReader.MoveEvent += PlayerMoveEvent;
        inputReader.JumpEvent += PlayerJumpEvent;

        playerStates = PlayerStates.Idle;
        currentPlayerState = playerStates;
    }

    private void OnDestroy()
    {
        GameManager.GameStartEvent -= PlayerCanMove;
        GameManager.GameStopEvent -= PlayerCantMove;

        inputReader.MoveEvent -= PlayerMoveEvent;
        inputReader.JumpEvent -= PlayerJumpEvent;
    }

    private void Update()
    {
        if (!canPlayerMove)
            return;
        

        switch (playerStates)
        {
            case PlayerStates.Idle:
                PlayerIdle();
                break;
            case PlayerStates.Walk:
                PlayerWalk();
                break;
            case PlayerStates.Jump:
                PlayerJump();
                break;
            case PlayerStates.Climb:
                PlayerClimb();
                break;

            default:
                PlayerIdle();
                break;
        }
    }

    private void PlayerClimb()
    {
        SetCurrentAnimation("PlayerClimb");
    }

    private void PlayerJump()
    {
        SetCurrentAnimation("PlayerJump");
        MoveHorizontal(3f);
   
    }

    private void PlayerWalk()
    {

        if(isPlayerOnGround)
           PlayerWalkMovement();
    }

    private void PlayerIdle()
    {
        SetCurrentAnimation("PlayerIdle");
    }

    private void SetCurrentAnimation(string AnimName)
    {
        if(animator.GetBool(AnimName))
            return;

        foreach(var anim in animator.parameters)
        {
            if(!anim.name.Equals(AnimName))
                animator.SetBool(anim.name, false);
        }
        animator.SetBool(AnimName, true);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (!canPlayerMove)
            return;

        FlipSprite();
        PlayerAnimation();
        Climb();
        //Jump();
    }

    private void PlayerWalkMovement()
    {
        if (moveImput != Vector2.zero)
        {
            newMovevector.x = moveImput.x;

            //animator.SetBool("PlayerWalk", true);
            acceleration += 2f * Time.deltaTime;
            //Debug.Log("aCCELERATION " + acceleration);
            if (acceleration > playerMoveSpeed)
                acceleration = playerMoveSpeed;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));

            //holdDownButton = true;
        }

        if (moveImput == Vector2.zero && acceleration > 2)
        {
            acceleration -= 15f * Time.deltaTime;
            //acceleration = 2;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));
        }
        else if (acceleration <= 2)
        {
            //animator.SetBool("PlayerWalk", false);
            acceleration = 2;
            rigidBody2D.velocity = Vector2.zero;
        }

    }

    private void MoveHorizontal(float acceleration)
    {
        
    }

    private void Jump()
    {
        if (!isJumping)
            return;


        rigidBody2D.velocity += Vector2.up * jumpForce;
        animator.SetBool("PlayerJump", true);
    }

    private void PlayerJumpEvent(bool jump)
    {
        if (!canPlayerMove)
            return;

        if (!isPlayerOnGround && !playerCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")))
            return;

        //isJumping = true;
        
        GravityReset();
        rigidBody2D.velocity += Vector2.up * jumpForce;
        //rigidBody2D.AddForce(Vector2.up * jumpForce);
        //animator.SetBool("PlayerJump", true);
        //SetCurrentAnimation("PlayerJump");
        playerStates = PlayerStates.Jump;


    }

    private void PlayerMoveEvent(Vector2 vector)
    {
        if (!canPlayerMove)
            return;

        moveImput = vector;
        FlipSprite();
    }

    private void PlayerCantMove()
    {
        canPlayerMove = false;
        rigidBody2D.velocity = Vector2.zero;
    }



    private void PlayerCanMove()
    {
        canPlayerMove = true;
    }

    private void Climb()
    {
        if (isJumping)
            return;

        if (!playerCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")))
        {
            if (animator.GetBool("PlayerClimb"))
                animator.SetBool("PlayerClimb", false);
            GravityReset();
            rigidBody2D.velocity = Vector2.down;
            return;
        }

        if (moveImput.y == 0f && !isJumping)
        {
            rigidBody2D.gravityScale = 0;
            rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, 0f);
            animator.SetBool("PlayerClimb", false);
        }
        else if (Mathf.Abs(moveImput.y) != 0 && !isJumping)
        {
            GravityReset();
            playerVelocity.y = moveImput.y;
            rigidBody2D.velocity = playerVelocity.normalized * playerMoveSpeed;
            animator.SetBool("PlayerClimb", true);
        }
        /* Player Climb using Collision detection 

        if (!canClimb)
        {
            animator.SetBool("PlayerClimb", false);
            return;
        }
        
        if (moveImput.y != 0)
        {
            rigidBody2D.gravityScale = baseGravityScale;
            playerVelocity.y = moveImput.y * playerMoveSpeed;
            rigidBody2D.velocity = playerVelocity;
            animator.SetBool("PlayerClimb", true);
        }

        if (canClimb && moveImput.y == 0 && !animator.GetBool("PlayerJump"))
        {
            playerVelocity.y = 0f;
            rigidBody2D.velocity = playerVelocity;
            animator.SetBool("PlayerClimb", false);
            rigidBody2D.gravityScale = 0f;
        }
*/
    }

    private void FlipSprite()
    {
        if (moveImput.x > 0)
            transformObject.localScale = new Vector2(-1f, transform.localScale.y);
        else if (moveImput.x < 0)
            transformObject.localScale = new Vector2(1f, transform.localScale.y);
    }

    private void Run()
    {

        if (isJumping)
            return;
        //if (moveImput == Vector2.zero)
        //    return;


        if (moveImput != Vector2.zero)
        {
            newMovevector.x = moveImput.x;

            //animator.SetBool("PlayerWalk", true);
            acceleration += 2f * Time.deltaTime;
            Debug.Log("aCCELERATION " + acceleration);
            if (acceleration > playerMoveSpeed)
                acceleration = playerMoveSpeed;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));

            //holdDownButton = true;
        }

        if (moveImput == Vector2.zero && acceleration > 2)
        {
            acceleration -= 15f * Time.deltaTime;
            //acceleration = 2;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));
        }
        else if (acceleration <= 2)
        {
            //animator.SetBool("PlayerWalk", false);
            acceleration = 2;
            rigidBody2D.velocity = Vector2.zero;
        }

        //PlayerAnimation();

        /*
        playerVelocity.x = moveImput.x * playerMoveSpeed;
        playerVelocity.y = rigidBody2D.velocity.y;
        animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));
        rigidBody2D.velocity = playerVelocity;
        */
    }

    private void PlayerAnimation()
    {
        /* Player Walk and jump using LayerMask tesr
        if (Math.Abs(playerVelocity.x) > 0 && !animator.GetBool("PlayerWalk") && playerCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
            animator.SetBool("PlayerWalk", true);
        else if (Math.Abs(playerVelocity.x) == 0 && animator.GetBool("PlayerWalk") || !playerCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground")))
            animator.SetBool("PlayerWalk", false);

            */


        if (Math.Abs(rigidBody2D.velocity.x) > 0 && !animator.GetBool("PlayerWalk") && isPlayerOnGround)
            animator.SetBool("PlayerWalk", true);
        else if (Math.Abs(rigidBody2D.velocity.x) == 0 && animator.GetBool("PlayerWalk") || !isPlayerOnGround)
            animator.SetBool("PlayerWalk", false);


    }

    private void OnMove(InputValue inputValue)
    {
        if (!canPlayerMove)
            return;
        moveImput = inputValue.Get<Vector2>();
        Debug.Log("PLayer move speed " + playerMoveSpeed * Time.deltaTime);

        FlipSprite();
    }

    private void OnJump(InputValue inputValue)
    {
        if (!canPlayerMove)
            return;
        if (!isPlayerOnGround && !playerCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")))
            return;

        if (inputValue.isPressed)
        {
            isJumping = true;
            GravityReset();
            rigidBody2D.velocity += Vector2.up * jumpForce;
            animator.SetBool("PlayerJump", true);
        }
    }
    /* Player Climb usunig colistion detection 
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag.Equals("Ladder")  && isPlayerOnGround)
                canClimb = true;

        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag.Equals("Ladder"))
                canClimb = false;
            animator.SetBool("PlayerClimb", false);
            rigidBody2D.gravityScale = baseGravityScale;
        }

    */
    // Player check if is on ground old method using OnCollision 

    private void GravityReset()
    {
        rigidBody2D.gravityScale = baseGravityScale;
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
        {
            isPlayerOnGround = true;
            isJumping = false;
            animator.SetBool("PlayerJump", false);
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
            isPlayerOnGround = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Ladder"))
        {
            //isJumping = false;
            //animator.SetBool("PlayerJump", false);
        }
    }



}
