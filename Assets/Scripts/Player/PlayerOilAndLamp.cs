using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerOilAndLamp : MonoBehaviour
{
    [SerializeField] private InputReader inputReader;
    [SerializeField] private float maxOilAmount;

    [SerializeField] private float oilAmount;

    private LampSetting lampSetting;

    private BarrelSetting barrelSetting;

    private bool canRefillLamp = false;

    private bool canGetOil = false;

    private bool gameRun = false;
    // Start is called before the first frame update

    private void Start() {
        GameManager.GameStartEvent += GameRunCanRefill;
        GameManager.GameStopEvent += GameStopCantRefill;
        inputReader.OnActionKeyEvent += OnRefillEvent;
    }

    private void OnDestroy() {
        GameManager.GameStartEvent -= GameRunCanRefill;
        GameManager.GameStopEvent -= GameStopCantRefill;
        inputReader.OnActionKeyEvent -= OnRefillEvent;
    }

    private void GameStopCantRefill()
    {
        gameRun = false;
    }

    private void GameRunCanRefill()
    {
        gameRun = true;
    }

    private void OnRefillEvent()
    {
        if ( canRefillLamp && gameRun)
        {
            float oiltoRefill = lampSetting.HowMuchOilMissing() < oilAmount ? lampSetting.HowMuchOilMissing() : oilAmount;
            oilAmount -= oiltoRefill;
            lampSetting.RefillOil(oiltoRefill);
            if(oilAmount <= 0)
            lampSetting.EnableDisableButton(false);
        }
        else if( canGetOil && gameRun)
        {
            barrelSetting.ColiedWithPlayerPlayAnimation(true);
            oilAmount += barrelSetting.GetOilFromBarrel(maxOilAmount - oilAmount);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Lamp"))
        {
            lampSetting = other.GetComponent<LampSetting>();
            if (oilAmount > 0){
                lampSetting.EnableDisableButton(true);
                canRefillLamp = true;
            }
        }

        if (other.tag.Equals("Barrel"))
        {
            canGetOil = true;
            barrelSetting = other.GetComponent<BarrelSetting>();
            barrelSetting.ColiedWithPlayerPlayAnimation(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag.Equals("Lamp"))
        {
            if (lampSetting != null)
            {
                lampSetting.EnableDisableButton(false);
                canRefillLamp = false;

            }
        }
        if (other.tag.Equals("Barrel"))
        {
            if (barrelSetting != null)
            {
                barrelSetting.ColiedWithPlayerPlayAnimation(false);
                canGetOil = false;
            }
        }

    }
}
