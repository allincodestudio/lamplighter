using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "States/Character/Idle")]

    public class IdleState : State<CharacterCtr>
    {

        private Rigidbody2D _rigidbody2D;

        private GroundCheck _groundCheck;

        private Animator _animator;

        private TestPlayerMove _testPlayerMove;

        private LadderCheck _ladderCheck;

        private Vector2 _inputXY;

        private bool _jump = false;

        private PlatformCheck _platformCheck;

        public override void Init(CharacterCtr parent)
        {
            base.Init(parent);

            if(_groundCheck == null) _groundCheck = parent.groundCheck;
            if(_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if(_animator == null) _animator = parent.animator;
            if(_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            if(_platformCheck == null) _platformCheck = parent.platformCheck;
            _testPlayerMove.PlayerStartJumpEvent += JumpEvent;
            _jump = false;
            /*
            _rigidbody2D = parent.rigidBody2D;
            _animator = parent.animator;
            _groundCheck = parent.groundCheck;
            _testPlayerMove = parent._testPlayerMove;
            */
        }

        private void JumpEvent(bool jump)
        {
            _jump = jump;
        }

        public override void AnimationStart()
        {
        }

        public override void CaptureInput()
        {
            _inputXY = _testPlayerMove.moveVector;
            //_input.x = Input.GetAxis("Horizontal");
            //_input.y = Input.GetAxis("Vertical");
            //_jump = _testPlayerMove.jumpValue;
            //_jump = Input.GetKey(KeyCode.Space);
        }

        public override void ChangeState()
        {
            if (_groundCheck.CheckCollision() | _platformCheck.CheckCollision() && _jump) _runner.SetState(typeof(JumpState));
            if (_groundCheck.CheckCollision() | _platformCheck.CheckCollision() && _inputXY.x != 0) _runner.SetState(typeof(WalkState));
            if (_groundCheck.CheckCollision() && _ladderCheck.LadderColideCheck() && _inputXY.y > 0) _runner.SetState(typeof(LadderClimbState));
            if (!_groundCheck.CheckCollision() && _ladderCheck.LadderColideCheck() && _inputXY.y < 0) _runner.SetState(typeof(LadderClimbState));
        }

        public override void Exit()
        {
            if(_jump) _jump = false;
        }

        public override void FixedUpdate()
        {
        }

        public override void Update()
        {
        }
    }
}

