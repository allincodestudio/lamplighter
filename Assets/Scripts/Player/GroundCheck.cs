using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    [SerializeField] private float _groundCheckRadius = 0.05f;

    [SerializeField] private LayerMask _collisionLayerMask;

    public bool CheckCollision()
    {

        return Physics2D.OverlapCircle(transform.position, _groundCheckRadius, _collisionLayerMask);
    }


}
