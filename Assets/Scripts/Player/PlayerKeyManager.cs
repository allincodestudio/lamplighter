
using UnityEngine;

public class PlayerKeyManager : MonoBehaviour
{

    [SerializeField] private int _normalKeyCounter = 0;

    [SerializeField] private int _specialKeyCounter = 0;
    // Start is called before the first frame update

    public int ReturnNormalKeyAmount()
    {
        return _normalKeyCounter;
    }

    public int ReturnSpecialKeyAmount()
    {
        return _specialKeyCounter;
    }

    public void RemoveNormalKey()
    {
        _normalKeyCounter--;
    }

    public void RemoveSpecialKey()
    {
        _specialKeyCounter--;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.GetComponent<IKeys>() != null)
        {
            if(other.GetComponent<KeyToCollect>().KeyValue() == KeysEnum.Keys.Normal)
            {
                _normalKeyCounter++;
                other.GetComponent<KeyToCollect>().KayCollected();
                return;
            }
            
            if(other.GetComponent<KeyToCollect>().KeyValue() == KeysEnum.Keys.Special)
            {
                _specialKeyCounter++;
                other.GetComponent<KeyToCollect>().KayCollected();
            }
        }
    }
}
