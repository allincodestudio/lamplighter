
using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "States/Character/Walk")]
    public class WalkState : State<CharacterCtr>
    {
        private Rigidbody2D _rigidbody2D;

        private GroundCheck _groundCheck;

        private Animator _animator;

        private SwitchSpriteSide _switchSpriteSide;

        private TestPlayerMove _testPlayerMove;

        private LadderCheck _ladderCheck;

        private Vector2 _inputXY;

        private Vector2 _moveVector;

        private bool _jump = false;

        private bool _canJump = true;

        //private PlayerStickToPlatform _playerStickToPlatform;

        private PlatformCheck _platformCheck;

        [SerializeField] private float _acceleration = 2f;

        [SerializeField] private float _exitTimer = 0.2f;

        private float _tmpExitTimer;

        private bool _run;

        [SerializeField] private float _runSpeed = 5f;

        [SerializeField] private float _walkSpeed = 3f;



        public override void Init(CharacterCtr parent)
        {
            base.Init(parent);
            if (_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if (_animator == null) _animator = parent.animator;
            if (_groundCheck == null) _groundCheck = parent.groundCheck;
            if (_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_switchSpriteSide == null) _switchSpriteSide = parent._switchSpriteSide;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            if(_platformCheck == null ) _platformCheck = parent.platformCheck;
            //if (_playerStickToPlatform == null) _playerStickToPlatform = parent.GetComponent<PlayerStickToPlatform>();
            _tmpExitTimer = _exitTimer;


        }

        public override void AnimationStart()
        {
            if (!_animator.GetBool("PlayerWalk")) _animator.SetBool("PlayerWalk", true);
        }

        public override void CaptureInput()
        {
            _inputXY = _testPlayerMove.moveVector;

            if (_inputXY != Vector2.zero)
                _moveVector = _inputXY;

            _testPlayerMove.PlayerStartJumpEvent += JumpEvent;
        }

        private void JumpEvent(bool jump)
        {
            _jump = jump;
        }

        public override void ChangeState()
        {
            if (!_groundCheck.CheckCollision() && !_platformCheck.CheckCollision() && !_canJump) _runner.SetState(typeof(FallState));
            if (_canJump && _jump) _runner.SetState(typeof(JumpState));

            if (_groundCheck.CheckCollision() && _ladderCheck.LadderColideCheck() && _inputXY.y > 0) _runner.SetState(typeof(LadderClimbState));
            if (!_groundCheck.CheckCollision() && _ladderCheck.LadderColideCheck() && _inputXY.y < 0) _runner.SetState(typeof(LadderClimbState));
            if (_groundCheck.CheckCollision() | _platformCheck.CheckCollision()  && Mathf.Abs(_rigidbody2D.velocity.x) <= Mathf.Epsilon) _runner.SetState(typeof(IdleState));
        }

        public override void Exit()
        {
            _animator.SetBool("PlayerWalk", false);
            if (_jump) _jump = false;
        }

        public override void FixedUpdate()
        {
            /*
            float platformMove = 0f;

            if(_inputXY.x == 0 || !_playerStickToPlatform.IsPlayerOnPlatform())
            platformMove = 0;
            else
            platformMove = _playerStickToPlatform.PlatformMoveSpeedReturn();
            */
            
            _run = _testPlayerMove.run;

            float platformMove = 1f;
            if(_platformCheck.CheckCollision())
            {
                platformMove = 2;
                _rigidbody2D.velocity = new Vector2(_moveVector.x * platformMove * AccelerationCountPlatform(), _rigidbody2D.velocity.y);
            }
            else
            {
               // _rigidbody2D.velocity = new Vector2(_moveVector.x * platformMove * AccelerationCount(), _rigidbody2D.velocity.y);
                _rigidbody2D.velocity = new Vector2(_inputXY.x * platformMove * _runSpeed, _rigidbody2D.velocity.y);
                if(_moveVector.x == 0)
                //_rigidbody2D.velocity = new Vector2(0, _rigidbody2D.velocity.y);

                Debug.Log(_moveVector.x);
            }
            _switchSpriteSide.SwitchSpriteSideX(_inputXY.x);
            if(!_platformCheck.CheckCollision())
            _animator.SetFloat("PlayerWalkRun", Mathf.Abs(_rigidbody2D.velocity.x));
            else
            _animator.SetFloat("PlayerWalkRun", Mathf.Abs(_rigidbody2D.velocity.x / 2f));
            if (!_canJump && _groundCheck.CheckCollision() | _platformCheck.CheckCollision())
                _canJump = true;
        }

        public override void Update()
        {
            if (!_groundCheck.CheckCollision() && _canJump)
                _tmpExitTimer -= Time.deltaTime;

            if (_tmpExitTimer < 0)
            {
                _canJump = false;
                _tmpExitTimer = _exitTimer;
            }

        }

        private float AccelerationCount()
        {

            if (_inputXY.x == 0 && _acceleration > 2)
            {
                _acceleration -= 0.3f - Time.deltaTime;
                return _acceleration;
            }
            else if (_inputXY.x == 0 && _acceleration <= 2)
            {
                _acceleration = 2f;
                _moveVector.x = 0f;
                //_rigidbody2D.velocity = new Vector2(0f, _rigidbody2D.velocity.y);
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration < _walkSpeed && !_run)
            {
                _acceleration += Time.deltaTime * 2;
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration > _walkSpeed && !_run)
            {
                _acceleration -= 0.3f - Time.deltaTime;
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration < _runSpeed && _run)
            {
                _acceleration += Time.deltaTime * 2;
                return _acceleration;
            }
            if (_moveVector.x != 0 && _acceleration > _runSpeed && _run)
            {
                _acceleration = _runSpeed;
                return _acceleration;
            }

            return _acceleration;


        }

          private float AccelerationCountPlatform()
        {

            if (_inputXY.x == 0 && _acceleration > 2)
            {
                _acceleration -= 0.3f - Time.deltaTime;
                return _acceleration;
            }
            else if (_inputXY.x == 0 && _acceleration <= 2)
            {
                _acceleration = 2f;
                _moveVector.x = 0f;
                //_rigidbody2D.velocity = new Vector2(0f, _rigidbody2D.velocity.y);
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration < _walkSpeed * 1.5f && !_run)
            {
                _acceleration += Time.deltaTime * 2;
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration > _walkSpeed * 1.5f && !_run)
            {
                _acceleration -= 0.3f - Time.deltaTime;
                return _acceleration;
            }

            if (_moveVector.x != 0 && _acceleration < _runSpeed * 1.5f && _run)
            {
                _acceleration += Time.deltaTime * 2;
                return _acceleration;
            }
            if (_moveVector.x != 0 && _acceleration > _runSpeed * 1.5f && _run)
            {
                _acceleration = _runSpeed;
                return _acceleration;
            }

            return _acceleration;


        }
    }
}
