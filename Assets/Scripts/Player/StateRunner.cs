using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace StateMachine
{
    public abstract class StateRunner<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] private List<State<T>> _states;

        private readonly Dictionary<Type, State<T>> _stateByType = new();

        private State<T> _activeState;

        public State<T> activeState{get => _activeState;}

        protected virtual void Awake() {
            _states.ForEach(state => _stateByType.Add(state.GetType(),state));
            SetState(_states[0].GetType());
        }

        public void SetState(Type newStateType)
        {
            if(_activeState != null)
                _activeState.Exit();

                _activeState = _stateByType[newStateType];
                _activeState.Init(parent:GetComponent<T>());
                _activeState.AnimationStart();
        }

        private void Update()
        {
            _activeState.CaptureInput();
            _activeState.Update();
            _activeState.ChangeState();
        }

        private void FixedUpdate()
        {
            _activeState.FixedUpdate();
        }
    }
}
