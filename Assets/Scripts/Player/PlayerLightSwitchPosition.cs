using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLightSwitchPosition : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;

    private float _lightPosition;

    private bool _switchLight = false;
    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponentInParent<SpriteRenderer>();
        _lightPosition = transform.localPosition.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(_switchLight == _spriteRenderer.flipX)
            return;

        _switchLight = _spriteRenderer.flipX;
        _lightPosition *= -1;

        transform.localPosition = new Vector3(_lightPosition, transform.localPosition.y, transform.localPosition.z);
    }
}
