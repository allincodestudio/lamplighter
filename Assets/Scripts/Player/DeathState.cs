using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
[CreateAssetMenu(menuName = "States/Character/Death")]
public class DeathState : State<CharacterCtr>
{

    public override void Init(CharacterCtr parent)
        {
            base.Init(parent);

            /*
            if (_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if (_animator == null) _animator = parent.animator;
            if (_groundCheck == null) _groundCheck = parent.groundCheck;
            if (_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_switchSpriteSide == null) _switchSpriteSide = parent._switchSpriteSide;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            if(_platformCheck == null ) _platformCheck = parent.platformCheck;
            //if (_playerStickToPlatform == null) _playerStickToPlatform = parent.GetComponent<PlayerStickToPlatform>();
            _tmpExitTimer = _exitTimer;

            */

            Debug.Log("Deatch State");


        }
        public override void AnimationStart()
        {
           
        }

        public override void CaptureInput()
        {
           
        }

        public override void ChangeState()
        {
           
        }

        public override void Exit()
        {
           
        }

        public override void FixedUpdate()
        {
           
        }

        public override void Update()
        {
           
        }

        // Start is called before the first frame update
        void Start()
    {
        
    }

    public System.Type SetDeathState()
    {
         return typeof(DeathState);
    }

}
}
