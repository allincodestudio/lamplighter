using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderCheck : MonoBehaviour
{
    private Collider2D _collider2D;

    private void Awake() {
        _collider2D = GetComponent<Collider2D>();
    }
    [SerializeField] private LayerMask _collisionLayerMask;
    // Start is called before the first frame update
    public bool LadderColideCheck()
    {
        return _collider2D.IsTouchingLayers(_collisionLayerMask);
    }
}
