using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{

    [CreateAssetMenu(menuName = "States/Character/Jump")]
    public class JumpState : State<CharacterCtr>
    {

        private GroundCheck _groundCheck;

        private Rigidbody2D _rigidbody2D;

        private SwitchSpriteSide _switchSpriteSide;

        private Animator _animator;

        private LadderCheck _ladderCheck;

        private bool _leftGround;

        private Vector2 _inputXY;

        [SerializeField] private float _speed = 23f;

        [SerializeField] private float _jumpVelocity = 10f;
        private TestPlayerMove _testPlayerMove;

        private float _jumpXSpped = 3f;

        private PlatformCheck _platformCheck;

        public override void Init(CharacterCtr parent)
        {
            base.Init(parent);
            if(_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if(_animator == null) _animator = parent.animator;
            if(_groundCheck == null) _groundCheck = parent.groundCheck;
            if(_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            if(_switchSpriteSide == null) _switchSpriteSide = parent._switchSpriteSide;
            if(_platformCheck == null) _platformCheck = parent.platformCheck;
            _leftGround = false;
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _jumpVelocity);
            _speed = Mathf.Abs(_rigidbody2D.velocity.x);
            if(_platformCheck.CheckCollision() && _speed > 5f)
            _speed = 5f;

            //if(_speed <= 0) _speed = _jumpXSpped / 2;

        }

        public override void AnimationStart()
        {
            if(!_animator.GetBool("PlayerJump")) _animator.SetBool("PlayerJump",true);
        }
        public override void CaptureInput()
        {
            _inputXY = _testPlayerMove.moveVector;
        }

        public override void ChangeState()
        {
            if(_leftGround && _groundCheck.CheckCollision() | _platformCheck.CheckCollision() && _rigidbody2D.velocity.x != 0 ) _runner.SetState(typeof(WalkState));
            if(_leftGround && _groundCheck.CheckCollision() | _platformCheck.CheckCollision() )  _runner.SetState(typeof(IdleState));
            if(_leftGround && _ladderCheck.LadderColideCheck() && _inputXY.y != 0) _runner.SetState(typeof(LadderClimbState));
        }

        public override void Exit()
        {
            _animator.SetBool("PlayerJump",false);
            _animator.SetBool("PlayerFall",false);
        }

        public override void FixedUpdate()
        {
              if(_speed > _jumpXSpped)
                _speed -= Time.deltaTime;

               if(_speed <= 0)
                {
                    _speed = 1;
                }
            _rigidbody2D.velocity = new Vector2( _inputXY.x * _speed , _rigidbody2D.velocity.y);
       
            _switchSpriteSide.SwitchSpriteSideX(_inputXY.x);
            if(_rigidbody2D.velocity.y < 0)
            {
                _animator.SetBool("PlayerFall",true);
                _animator.SetBool("PlayerJump",false);
            }

            if(!_groundCheck.CheckCollision() && !_platformCheck.CheckCollision() && !_leftGround)
                _leftGround = true;
        }

        public override void Update()
        {
                // if(_speed > _jumpXSpped)
                //_speed -= Time.deltaTime;
            //_rigidbody2D.velocity = new Vector2( _inputXY.x * _speed * 10, _rigidbody2D.velocity.y);
           // if(_inputXY.x != 0)
            //{
            //    _rigidbody2D.AddForce(new Vector2(_inputXY.x * 10,_rigidbody2D.velocity.y));
            //    Debug.Log("Input X = " + _inputXY.x);
            //}
        }
    }

}
