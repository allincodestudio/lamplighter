using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDoorOpen : MonoBehaviour
{
    [SerializeField] private InputReader _inputReader;

    //private bool _caOpenDoor = false;

    private PlayerKeyManager _playerKeyManager;

    private DoorOpening _doorOpening;

    private void Start() {
        _inputReader.OnActionKeyEvent += OpenDoorEvent;

        _playerKeyManager = GetComponent<PlayerKeyManager>();
    }

    private void OnDisable() {
        _inputReader.OnActionKeyEvent -= OpenDoorEvent;
    }

    private void OpenDoorEvent()
    {
        if(_doorOpening != null )
        {
            if(_playerKeyManager.ReturnNormalKeyAmount() > 0 && _doorOpening.DoorValue() == KeysEnum.Keys.Normal){
            _doorOpening.OpenDoor();
            _playerKeyManager.RemoveNormalKey();
            return;
            }

            if(_playerKeyManager.ReturnSpecialKeyAmount() > 0 && _doorOpening.DoorValue() == KeysEnum.Keys.Special)
            {
                _doorOpening.OpenDoor();
                _playerKeyManager.RemoveSpecialKey();
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.GetComponent<DoorOpening>() != null)
        {
            _doorOpening = other.GetComponent<DoorOpening>();
            //_caOpenDoor = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        _doorOpening = null;
    }
}
