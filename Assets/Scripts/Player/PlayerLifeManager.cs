using System;
using System.Collections;
using System.Collections.Generic;
using StateMachine;
using UnityEngine;

public class PlayerLifeManager : MonoBehaviour
{

    [SerializeField] private DeathState _deathState;
    private Animator animator;

    private CharacterCtr _playerCharacterCtr;

    private void Start() {
        animator = GetComponent<Animator>();
        _playerCharacterCtr = GetComponent<CharacterCtr>();
    }
   private void OnCollisionEnter2D(Collision2D other) {
    if(other.gameObject.tag.Equals("Enemy"))
    {
        Debug.Log("Player Die");
        PlayerDeath();
        GameManager.gameManageInstance.PlayerDeatch();

        _playerCharacterCtr.SetState(_deathState.SetDeathState());
        
    }
   }

   private void OnTriggerEnter2D(Collider2D other) {
    if(other.tag.Equals("Enemy"))
    {
        Debug.Log("Player Die");
        PlayerDeath();
        GameManager.gameManageInstance.PlayerDeatch();

        _playerCharacterCtr.SetState(_deathState.SetDeathState());
    }
   }

   private void PlayerDeath()
   {
    animator.SetBool("PlayerWalk",false);
    animator.SetBool("PlayerJump",false);
    animator.SetBool("PlayerClimb",false);
    animator.SetBool("PlayerDeath",true);
   }
}
