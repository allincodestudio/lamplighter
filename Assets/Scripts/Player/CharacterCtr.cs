using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace StateMachine
{

    public class CharacterCtr : StateRunner<CharacterCtr>
    {

        internal SwitchSpriteSide _switchSpriteSide;

        private TestPlayerMove _testPlayerMove;

        private Rigidbody2D _rigidbody2D;

        private GroundCheck _groundCheck;

        private PlatformCheck _platformCheck;

        private LadderCheck _ladderCheck;

        private Animator _animator;

        private CinemachineImpulseSource _cinemachineImpulseSource;

        private PlayerStickToPlatform _playerStickToPlatform;

        internal Rigidbody2D rigidBody2D { get => _rigidbody2D; set => _rigidbody2D = value; }
        internal Animator animator { get => _animator; set => _animator = value; }

        internal GroundCheck groundCheck { get => _groundCheck; }

        internal PlatformCheck platformCheck{get => _platformCheck;}

        internal TestPlayerMove testPlayerMove{get => _testPlayerMove; set => _testPlayerMove = value;}

        internal LadderCheck ladderCheck{get => _ladderCheck;}

        internal CinemachineImpulseSource cinemachineImpulseSource{get => _cinemachineImpulseSource; set => _cinemachineImpulseSource = value;} 

        protected override void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            _groundCheck = GetComponentInChildren<GroundCheck>();
            _switchSpriteSide = new SwitchSpriteSide(GetComponent<SpriteRenderer>());
            _ladderCheck = GetComponent<LadderCheck>();
            _cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
            _playerStickToPlatform = GetComponent<PlayerStickToPlatform>();
            _platformCheck = GetComponentInChildren<PlatformCheck>();

            base.Awake();
        }

    }
}
