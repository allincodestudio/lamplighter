using System;
using System.Collections;
using System.Collections.Generic;
using Unity.IO.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;

namespace StateMachine
{
    [CreateAssetMenu(menuName = "States/Character/LedderClimb")]
    public class LadderClimbState : State<CharacterCtr>
    {
        [SerializeField] private float ladderClimbSpeed = 3f;
        private Rigidbody2D _rigidbody2D;

        private GroundCheck _groundCheck;

        private Animator _animator;

        private TestPlayerMove _testPlayerMove;

        private LadderCheck _ladderCheck;

        private bool _leftGround;

        private Vector2 _inputXY;

        private float _gravityScale;

        private bool _jump;

        private float _animationSpeed;

        public override void Init(CharacterCtr parent)
        {
            base.Init(parent);

            if (_rigidbody2D == null) _rigidbody2D = parent.rigidBody2D;
            if (_animator == null) _animator = parent.animator;
            if (_groundCheck == null) _groundCheck = parent.groundCheck;
            if (_ladderCheck == null) _ladderCheck = parent.ladderCheck;
            if (_testPlayerMove == null) _testPlayerMove = parent.GetComponent<TestPlayerMove>();
            _testPlayerMove.PlayerStartJumpEvent += JumpEvent;
            _leftGround = false;
            _gravityScale = _rigidbody2D.gravityScale;
            _rigidbody2D.gravityScale = 0f;
            _animationSpeed = _animator.speed;
        }

        private void JumpEvent(bool jump)
        {
            _jump = jump;
        }

        public override void AnimationStart()
        {
            if (!_animator.GetBool("PlayerClimb")) _animator.SetBool("PlayerClimb", true);
        }

        public override void CaptureInput()
        {
            _inputXY = _testPlayerMove.moveVector;
        }

        public override void ChangeState()
        {
            if (_groundCheck.CheckCollision() && _rigidbody2D.velocity.x != 0) _runner.SetState(typeof(WalkState));
            if (_groundCheck.CheckCollision() && _rigidbody2D.velocity == Vector2.zero) _runner.SetState(typeof(IdleState));
            if (_jump) _runner.SetState(typeof(JumpState));
            if (!_groundCheck.CheckCollision() && !_ladderCheck.LadderColideCheck()) _runner.SetState(typeof(FallState));
        }

        public override void Exit()
        {
            _animator.SetBool("PlayerClimb", false);

            _rigidbody2D.gravityScale = _gravityScale;
        }

        public override void FixedUpdate()
        {
            _rigidbody2D.velocity = _inputXY * ladderClimbSpeed;

            if (!_groundCheck.CheckCollision() && !_leftGround)
                _leftGround = true;

            _animator.SetFloat("PlayerClimbValue", Mathf.Abs(_rigidbody2D.velocity.y));
        }

        public override void Update()
        {
        }
    }
}
