using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
public class SwitchSpriteSide 
{
    private SpriteRenderer _spriteRenderer;

    public SwitchSpriteSide(SpriteRenderer renderer)
    {
        _spriteRenderer = renderer;
    }

    public void SwitchSpriteSideX(float xValue)
    {
        if(xValue > 0 )
            _spriteRenderer.flipX = true;
        else if( xValue < 0)
            _spriteRenderer.flipX = false;
    }
}

}
