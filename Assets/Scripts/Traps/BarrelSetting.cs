using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelSetting : MonoBehaviour
{
    [SerializeField] private Sprite barrelWithOil;

    [SerializeField] private Sprite emptyBarell;

    [SerializeField] private float oilAmountInBarrel;

    [SerializeField] private SpriteRenderer buttonSpriteRenderer;

    [SerializeField] private Animator buttonAnimator;


    private void Start()
    {

        barrelWithOil = this.GetComponent<SpriteRenderer>().sprite;
        buttonSpriteRenderer.enabled = false;
        buttonAnimator.enabled = false;
    }

    public float GetOilFromBarrel(float amountToGet)
    {
        if (amountToGet < oilAmountInBarrel)
        {
            oilAmountInBarrel -= amountToGet;
            return amountToGet;
        }
        else if (oilAmountInBarrel > 0)
        {
            amountToGet = oilAmountInBarrel;
            oilAmountInBarrel = 0f;
            this.GetComponent<SpriteRenderer>().sprite = emptyBarell;
            ColiedWithPlayerPlayAnimation(false);
            return amountToGet;
        }
        return 0f;
    }

    public void ColiedWithPlayerPlayAnimation(bool buttonStatus)
    {
        if (oilAmountInBarrel <= 0)
        {
            buttonStatus = false;
        }
        buttonSpriteRenderer.enabled = buttonStatus;
        buttonAnimator.enabled = buttonStatus;

    }
}
