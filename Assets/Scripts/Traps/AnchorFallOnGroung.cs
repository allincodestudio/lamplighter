using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class AnchorFallOnGroung : MonoBehaviour
{
    private Rigidbody2D rigidBody2D;
    private Animator anchorAnimator;

    private Collider2D[] collider2Ds;

    private CinemachineImpulseSource _cinemachineImpulseSource;

    private void Awake()
    {
        anchorAnimator = GetComponent<Animator>();
        rigidBody2D = GetComponent<Rigidbody2D>();
        collider2Ds = GetComponents<Collider2D>();
        _cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();
        rigidBody2D.gravityScale = 0f;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.gameObject.tag.Equals("Ground"))
        {
            _cinemachineImpulseSource.GenerateImpulse();
            transform.Translate(Vector2.zero);
            //rigidBody2D.gravityScale = 0f;
            rigidBody2D.excludeLayers = LayerMask.GetMask("Player");

            /*
                        foreach(var collider in collider2Ds)
                        {
                            collider.enabled = false;
                        }
                        */

        }
    }

    public void EnableAnchorFalling()
    {
        rigidBody2D.gravityScale = 2f;
    }
}
