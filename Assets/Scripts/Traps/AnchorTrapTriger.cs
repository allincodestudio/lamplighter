using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorTrapTriger : MonoBehaviour
{
    [SerializeField] private AnchorFallOnGroung anchorFallOnGroung;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            anchorFallOnGroung.EnableAnchorFalling();
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
            this.gameObject.SetActive(false);
        }
    }

}
