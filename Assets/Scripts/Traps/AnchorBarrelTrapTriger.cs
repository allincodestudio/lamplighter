using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorBarrelTrapTriger : MonoBehaviour
{
    // Start is called before the first frame update
 [SerializeField] private AnchorFallOnGroung anchorFallOnGroung;

 private Animator animator;

 private void Start() {
    animator = GetComponent<Animator>();
 }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            anchorFallOnGroung.EnableAnchorFalling();
            //transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
            //this.gameObject.SetActive(false);
            //animator.SetBool("Crush",true);
        }

        if (other.GetComponent<AnchorFallOnGroung>() != null)
        {
            //anchorFallOnGroung.EnableAnchorFalling();
            //transform.position = new Vector3(transform.position.x, transform.position.y - 0.05f, transform.position.z);
            //this.gameObject.SetActive(false);
            animator.SetBool("Crush",true);
        }
    }
}
