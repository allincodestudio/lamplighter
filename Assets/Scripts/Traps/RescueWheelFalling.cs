using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RescueWheelFalling : MonoBehaviour
{

    private BoxCollider2D boxCollider2D;
    private CircleCollider2D circleCollider2D;

    private Animator animator;

    private Rigidbody2D rigidBody2D;

    private bool isInRange = false;

    private bool isMoving = false;
    private Transform playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
        circleCollider2D = GetComponent<CircleCollider2D>();
        animator = GetComponentInChildren<Animator>();
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if(isMoving)
        CheckIfNotStoped();

        if (!isInRange)
            return;

        CheckDistanceToPlayer();
    }

    private void CheckIfNotStoped()
    {
        if(rigidBody2D.velocity != Vector2.zero)
        return;

        isMoving = false;
        rigidBody2D.gravityScale = 0f;
        circleCollider2D.enabled = false;


    }

    private void CheckDistanceToPlayer()
    {
        if (Mathf.Abs(playerTransform.position.x - transform.position.x) < 2f)
            ReleaseTheWheel();

    }

    private void ReleaseTheWheel()
    {
        //rigidBody2D.AddForce(Vector2.left,ForceMode2D.Impulse);
        rigidBody2D.AddForce((playerTransform.position - transform.position) * 1.5f ,ForceMode2D.Impulse) ;
        AnimatorMange(false);
        boxCollider2D.enabled = false;
        rigidBody2D.gravityScale = 1f;
        isInRange = false;
        isMoving = true;
        this.gameObject.tag = "Enemy";
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            isInRange = true;
            playerTransform = other.transform;
            AnimatorMange(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            isInRange = false;
            playerTransform = null;
            AnimatorMange(false);
        }
    }

    private void AnimatorMange(bool status)
    {
        animator.SetBool("Vibrate", status);
    }
}
