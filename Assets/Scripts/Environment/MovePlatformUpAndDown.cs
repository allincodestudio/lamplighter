using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class MovePlatformUpAndDown : MonoBehaviour
{
    [SerializeField] private bool _moveDown = true;
    [SerializeField] private float _moveLenght;

    [SerializeField] private float _moveSpeed;

    private Vector2 _startPosition;
    private Vector2 _endPosition;

    private Vector2 _moveVector = Vector2.zero;


    // Start is called before the first frame update
    void Start()
    {
        _startPosition = transform.position;
        _endPosition = new Vector2(_startPosition.x, _startPosition.y + _moveLenght);

        if(_moveDown)
        {
            _endPosition = transform.position;
            _startPosition = new Vector2(transform.position.x, transform.position.y - _moveLenght);
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {

            if (transform.position.y > _endPosition.y && _moveSpeed > 0)
            {
                _moveSpeed *= -1;
                return;
            }

            if (transform.position.y < _startPosition.y && _moveSpeed < 0)
                _moveSpeed *= -1;
    }

    private void Update()
    {
        _moveVector = Vector2.up * _moveSpeed * Time.deltaTime;
        transform.Translate(_moveVector);
    }

    public Vector2 ReturnMoveVectorUpAndDown()
    {
        return _moveVector;
    }
}
