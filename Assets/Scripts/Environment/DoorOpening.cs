
using UnityEngine;

public class DoorOpening : MonoBehaviour
{
    
    [SerializeField] private KeysEnum.Keys _doorKey;
    [SerializeField] private GameObject _qKey;

    private Animator _animator;

    private SpriteRenderer _qKeySprite;

    private SpriteRenderer _spriteRenderer;

    private Collider2D[] _collider2D;

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _qKeySprite = _qKey.GetComponent<SpriteRenderer>();

        _collider2D = GetComponents<Collider2D>();

        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {

        if (_qKeySprite != null)
            _qKeySprite.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            _qKeySprite.enabled = true;
            if(other.transform.position.x < transform.position.x)
                SwitchSpriteSide();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _qKeySprite.enabled = false;
    }

    private void SwitchSpriteSide()
    {
        _spriteRenderer.flipX = true;
    }

    public void OpenDoor()
    {
        _animator.SetBool("OpenDoor", true);
        foreach(Collider2D collider2D in _collider2D)
        {
            collider2D.enabled = false;
        }

    }

    public KeysEnum.Keys DoorValue()
    {
        return _doorKey;
    }



}
