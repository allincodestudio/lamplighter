using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyToCollect : MonoBehaviour, IKeys
{
   [SerializeField]private KeysEnum.Keys _keys;

    public KeysEnum.Keys KeyValue()
    {
        return _keys;
    }

    public void KayCollected()
    {
        this.gameObject.SetActive(false);
    }
}
