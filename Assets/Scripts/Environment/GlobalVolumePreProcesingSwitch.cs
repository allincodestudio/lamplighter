using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class GlobalVolumePreProcesingSwitch : MonoBehaviour
{

    private static GlobalVolumePreProcesingSwitch globalVolumePreProcesingSwitch;

    public static GlobalVolumePreProcesingSwitch globalVolumePreProcesingSwitchInsance{
        get {
            if(globalVolumePreProcesingSwitch == null)
                globalVolumePreProcesingSwitch = new GlobalVolumePreProcesingSwitch();

                return globalVolumePreProcesingSwitch;

        }
    }

    [SerializeField] private VolumeProfile _defaultPreProcessingVolume;
    [SerializeField] private VolumeProfile _deatchGhostPreProcessingVolume;

    [SerializeField] private bool _switchVolume = false;

    private Volume _globalVolume;

    private void Awake()
    {
        if(globalVolumePreProcesingSwitch == null)
        {
            globalVolumePreProcesingSwitch = this;
            DontDestroyOnLoad(this);
        }
        else
        Destroy(this.gameObject);

        _globalVolume = GetComponent<Volume>();
    }

    void Start()
    {
        
        _globalVolume.profile = _defaultPreProcessingVolume;
    }

    public void SwitchPreProcesing(bool status)
    {
        if(_globalVolume == null)
        return;
        if(status) _globalVolume.profile = _deatchGhostPreProcessingVolume;
        else
        _globalVolume.profile = _defaultPreProcessingVolume;
    }
}
