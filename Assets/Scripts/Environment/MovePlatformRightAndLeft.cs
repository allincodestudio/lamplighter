using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MovePlatformRightAndLeft : MonoBehaviour
{

    [SerializeField] private bool _moveLeft;
    [SerializeField] private float _moveLenght;

    [SerializeField] private float _moveSpeed;

    private GameObject _playerGameObject;

    private Vector2 _startPosition;
    private Vector2 _endPosition;

    private Vector2 _moveVector = Vector2.zero;

    public float moveSpeed { get => _moveSpeed; }


    // Start is called before the first frame update
    void Start()
    {
        _startPosition = transform.position;
        _endPosition = new Vector2(_startPosition.x + _moveLenght, _startPosition.y);

        if (_moveLeft)
        {
            _startPosition = new Vector2(transform.position.x - _moveLenght, transform.position.y);
            _endPosition = transform.position;
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (transform.position.x > _endPosition.x && _moveSpeed > 0)
        {
            _moveSpeed *= -1;
            return;
        }

        if (transform.position.x < _startPosition.x && _moveSpeed < 0)
            _moveSpeed *= -1;
    }

/*
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.SetParent(this.transform);
            _playerGameObject = other.gameObject;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
            other.transform.SetParent(null);
    }

    */



    private void Update()
    {
        _moveVector = Vector2.right * _moveSpeed * Time.deltaTime;
        transform.Translate(_moveVector);
    }

    public Vector2 ReturnMoveVectorRightAndLeft()
    {
        return _moveVector;
    }

 
}
