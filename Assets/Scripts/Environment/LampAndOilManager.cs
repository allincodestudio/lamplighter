using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampAndOilManager : MonoBehaviour
{
    private static LampAndOilManager lampAndOilManager;

    public static LampAndOilManager lampAndOilManagerInstance
    {
        get
        {
            if (lampAndOilManager == null)

                lampAndOilManager = new LampAndOilManager();

            return lampAndOilManager;
        }

        set { lampAndOilManager = value; }
    }

    private int lampsOn = 0;

    private float oilLeft = 0f;

    private float playerOilLeft = 0f;

    [SerializeField] private float timeToLauchDeatchGhost = 30f;

    [SerializeField] private GameObject deathGhostPrefab;

    private float timeCoundown = 0;

    private bool countDown = false;

    private GameObject deathGhost;

    private void Awake() {
        if(lampAndOilManager == null)
        {
            lampAndOilManager = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    private void OnEnable() {
        InstantiateDeathGhost();
    }
    // Start is called before the first frame update
    void Start()
    {
        ResetCountDownTimer();
        //InstantiateDeathGhost();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (countDown)
            CountingDown();
    }

    private void CountingDown()
    {
        timeCoundown -= Time.deltaTime;
        if (timeCoundown <= 0)
        {
            countDown = false;
            ResetCountDownTimer();
           // LaunchDeathGhost();
        }
    }

    private void InstantiateDeathGhost()
    {
        deathGhost = Instantiate(deathGhostPrefab) as GameObject;
        deathGhost.GetComponent<DeathGhostControler>().GhostEnableDisableStatus(false);
        //deathGhost.GhostEnableDisableStatus(false);
    }

    private void LaunchDeathGhost()
    {
        //if(deathGhost == null || deathGhost.gameObject.activeSelf)
        //return;
        if( deathGhost.gameObject.activeSelf)
        return;
        //deathGhost.GhostEnableDisableStatus(true);
        //deathGhost.GetComponent<DeathGhostControler>().GhostEnableDisableStatus(true);
        deathGhost.GetComponent<DeathGhostControler>().GhostEnableDisableStatus(true);
    }

    private void DisableDeathGhost()
    {
        //if(deathGhost == null || !deathGhost.gameObject.activeSelf)
        //return;
        if(!deathGhost.gameObject.activeSelf)
        return;
        //deathGhost.GhostEnableDisableStatus(false);
        //deathGhost.GetComponent<DeathGhostControler>().GhostEnableDisableStatus(false);
        deathGhost.GetComponent<DeathGhostControler>().GhostEnableDisableStatus(false);
    }

    public void LampOnAdd()
    {
        lampsOn++;
       // Debug.Log(lampsOn);
        //CheckLampOnAndOilLeft();
        if(lampsOn > 0 && deathGhost != null)
        DisableDeathGhost();
    }

    public void LampOnRemove()
    {
        lampsOn--;
        //Debug.Log(lampsOn);
        //CheckLampOnAndOilLeft();
        if(lampsOn <= 0)
        LaunchDeathGhost();
    }

    private void CheckLampOnAndOilLeft()
    {
        if (lampsOn <= 0)
        {
            LaunchDeathGhost();
            return;
        }
        DisableDeathGhost();
    }

    private void ResetCountDownTimer()
    {
        timeCoundown = timeToLauchDeatchGhost;
    }
}
