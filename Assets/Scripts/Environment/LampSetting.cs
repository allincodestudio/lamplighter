using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class LampSetting : MonoBehaviour
{

    [SerializeField] private GameObject buttonToPress;

    [SerializeField] private Light2D lampLight2D;

    [SerializeField] private Slider lampSlider;

    [SerializeField] private float oilQuantity;

    [SerializeField] private float maxOilQuantity;

    private SpriteRenderer buttonSpriteRenderer;

    private Animator buttonAnimation;

    private void Awake()
    {

    }

    void Start()
    {
        buttonAnimation = buttonToPress.GetComponent<Animator>();
        buttonSpriteRenderer = buttonToPress.GetComponent<SpriteRenderer>();

        buttonSpriteRenderer.enabled = false;
        buttonAnimation.enabled = false;
        //maxOilQuantity = oilQuantity;

        if (oilQuantity > 0)
        {
            LampAndOilManager.lampAndOilManagerInstance.LampOnAdd();
        }
        else
        {
            lampLight2D.enabled = false;
        }

    }

    void Update()
    {
        SliderUpdate();

        BurnOutOil();
    }

    private void SliderUpdate()
    {
        lampSlider.value = oilQuantity / maxOilQuantity;
    }

    private void BurnOutOil()
    {
        if (oilQuantity > 0)
        {
            oilQuantity -= Time.deltaTime;
        }
        else
        {
            DisableLight();
        }
    }

    public void EnableDisableButton(bool buttonState)
    {
        buttonSpriteRenderer.enabled = buttonState;
        buttonAnimation.enabled = buttonState;
    }

    public float HowMuchOilMissing()
    {
        return maxOilQuantity - oilQuantity;
    }

    public void RefillOil(float oilAmount)
    {
        oilQuantity += oilAmount;
        oilQuantity = oilQuantity > maxOilQuantity ? maxOilQuantity : oilQuantity;
        if (lampLight2D.enabled)
            return;

        lampLight2D.enabled = true;
        LampAndOilManager.lampAndOilManagerInstance.LampOnAdd();
    }

    private void DisableLight()
    {
        if (!lampLight2D.enabled)
            return;

        lampLight2D.enabled = false;
        LampAndOilManager.lampAndOilManagerInstance.LampOnRemove();
    }

    public bool ReturnStatusOfLamp()
    {
        return lampLight2D.enabled;
    }

}
