using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private static GameManager gameManager;

    public static GameManager gameManageInstance{
        get{
        if(gameManager == null)
        {
            gameManager = new GameManager();
        }

        return gameManager;
        }
    }


    public static event Action GameStartEvent;

    public static event Action GameStopEvent;

    private bool isSceneLoaded = false;
    private void Awake() {
        if(gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    private void Start() {
        isSceneLoaded = false;
    }

    private void Update() {
        if(isSceneLoaded)
            return;

        if(SceneManager.GetActiveScene().isLoaded)
        {
            isSceneLoaded = true;
            GameStartEvent?.Invoke();
            Debug.Log("Game Start event");
        }
    }

    public void PlayerDeatch()
    {
        GameStopEvent?.Invoke();
    }
  
}
