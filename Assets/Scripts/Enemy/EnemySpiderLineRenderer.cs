using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpiderLineRenderer : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private List<Vector3> pos = new List<Vector3>();

    private bool renderStatus = false;

    private Vector2 startPoint = Vector2.zero;
    private void Start() {
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       // if(renderStatus)
        //RenderSpiderLine();
    }

    public void RenderSpiderLine()
    {
        pos.Add(startPoint + Vector2.up /4);
        pos.Add(transform.position + Vector3.up /4);
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.SetPositions(pos.ToArray());
        lineRenderer.useWorldSpace = true;
        lineRenderer.enabled = true;
        pos.Clear();
    }

    public void RenderSpiderLineValues(bool status, Vector2 startPoint)
    {
        this.startPoint = startPoint;
        renderStatus = status;
        lineRenderer.enabled = lineRenderer != status ? status : lineRenderer.enabled;
        if(status)
            RenderSpiderLine();
    }
}
