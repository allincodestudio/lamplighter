using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathGhostControler : MonoBehaviour
{

    private GameObject player;
    [SerializeField] private float moveSpeed = 1;

    private SpriteRenderer spriteRenderer;

    private bool canMove = false;

    private Vector3 moveVector;

    private Vector3 positionOffset = new Vector3(0f,5f,0f);

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        player = FindObjectOfType<PlayerLifeManager>().gameObject;
        canMove = true;
    }


    // Update is called once per frame
    void Update()
    {
        if (player == null || !canMove)
            return;

        MoveGhostInToPlayerAndSwitchSprite();
    }

    private void MoveGhostInToPlayerAndSwitchSprite()
    {
        moveVector = (player.transform.position - transform.position).normalized;
        transform.Translate(moveVector * moveSpeed * Time.deltaTime);
        if (moveVector.x > 0 && !spriteRenderer.flipX)
        spriteRenderer.flipX = true;

        if(moveVector.x < 0 && spriteRenderer.flipX)
        spriteRenderer.flipX = false;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            canMove = false;
        }
    }

    private void SetGhostPosition()
    {
        transform.position = player.transform.position + positionOffset;
    }

    public void GhostEnableDisableStatus(bool status)
    {
        this.gameObject.SetActive(status);
        GlobalVolumePreProcesingSwitch.globalVolumePreProcesingSwitchInsance.SwitchPreProcesing(status);
        canMove = status;
        if(status)
            SetGhostPosition();
        else
        transform.position = new Vector3(-10,-10,0);

    }
}
