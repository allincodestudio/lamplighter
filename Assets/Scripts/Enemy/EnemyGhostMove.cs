using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class EnemyGhostMove : MonoBehaviour
{
    [SerializeField] private float enemyMoveLength;
    [SerializeField] private float enemyMoveSpeed;
    [SerializeField] private float enemyChaseSpeed;
    [SerializeField] private float enemyAtackSpeed;
    [SerializeField] private float enemyRayLength;
    [SerializeField] private float enemyWaitTime;

    private Vector2 enemyStartPosition;

    private Vector2 enemyChaseStartPoint;

    private Rigidbody2D rigidBody2D;

    private Vector2 moveVector = Vector2.zero;

    private Vector2 playerPosition;

    private RaycastHit2D[] raycastHit2D = new RaycastHit2D[3];

    private GhostEnemyStates enemyState;

    private float timeWaitCounter = 0;

    void Start()
    {
        enemyStartPosition = transform.position;
        rigidBody2D = GetComponent<Rigidbody2D>();
        UpdateEnemyState(GhostEnemyStates.Patrol);
    }

    void Update()
    {
        UpdateEnemyState(enemyState);
    }


    private void UpdateEnemyState(GhostEnemyStates newEnemyState)
    {
        enemyState = newEnemyState;

        switch (enemyState)
        {
            case GhostEnemyStates.Patrol:
                EnemyPatrol();
                enemyChaseStartPoint = transform.position;
                break;
            case GhostEnemyStates.Chase:
                EnemyChase();
                EnemyWaitTimeCounterReset();
                break;
            case GhostEnemyStates.Atack:
                EnemyWaitTimeCounterReset();
                break;
            case GhostEnemyStates.Wait:
                EnemyWait();
                break;
            case GhostEnemyStates.MoveBack:
                EnemyMoveBack();
                EnemyWaitTimeCounterReset();
                break;
            case GhostEnemyStates.Hide:
                EnemyHide();
                EnemyWaitTimeCounterReset();
                break;
            default:
                enemyState = GhostEnemyStates.Patrol;
                break;
        }

    }
    #region //Enemy States
    private void EnemyPatrol()
    {
        if (transform.position.x >= enemyStartPosition.x + enemyMoveLength)
            moveVector = new Vector2(-1, 0f).normalized * enemyMoveSpeed;

        if (transform.position.x <= enemyStartPosition.x)
            moveVector = new Vector2(1, 0f).normalized * enemyMoveSpeed;
        SpriteSideSwitch(moveVector);
        rigidBody2D.velocity = moveVector;
    }

    private void EnemyChase()
    {
        if (playerPosition == Vector2.zero)
        {
            UpdateEnemyState(GhostEnemyStates.Wait);
            return;
        }
        rigidBody2D.velocity = new Vector2(playerPosition.x - transform.position.x, playerPosition.y - transform.position.y).normalized * enemyChaseSpeed;
    }

    private void EnemyWait()
    {
        timeWaitCounter += Time.deltaTime;
        rigidBody2D.velocity = Vector2.zero;

        if (timeWaitCounter >= enemyWaitTime)
        {
            EnemyWaitTimeCounterReset();
            UpdateEnemyState(GhostEnemyStates.MoveBack);
        }
    }

    private void EnemyMoveBack()
    {
        if (Vector2.Distance(transform.position, enemyStartPosition) < 0.1f)
        {
            UpdateEnemyState(GhostEnemyStates.Patrol);
            return;
        }
        moveVector = new Vector2(enemyStartPosition.x - transform.position.x, enemyChaseStartPoint.y - transform.position.y).normalized * enemyMoveSpeed;
        rigidBody2D.velocity = moveVector;
        SpriteSideSwitch(moveVector);
    }

    private void EnemyHide()
    {
        rigidBody2D.velocity = Vector2.zero;
    }

    private void EnemyWaitTimeCounterReset()
    {
        timeWaitCounter = 0;
    }
    #endregion

    private void SpriteSideSwitch(Vector2 sideVector)
    {
        transform.localScale = new Vector2(MathF.Sign(sideVector.x), 1f);
    }

    private void FixedUpdate()
    {
        raycastHit2D[0] = Physics2D.Raycast(transform.position, moveVector * 3, enemyRayLength, LayerMask.GetMask("Player"));
        raycastHit2D[1] = Physics2D.Raycast(transform.position, new Vector3(moveVector.x, moveVector.y - 1, 0f) * 2, enemyRayLength, LayerMask.GetMask("Player"));
        raycastHit2D[2] = Physics2D.Raycast(transform.position, new Vector3(moveVector.x, moveVector.y + 1, 0f) * 2, enemyRayLength, LayerMask.GetMask("Player"));
        Debug.DrawRay(transform.position, moveVector * 3, Color.red, 0.1f, true);
        Debug.DrawRay(transform.position, new Vector3(moveVector.x, moveVector.y - 1, 0f) * 2, Color.red, 0.1f, true);
        Debug.DrawRay(transform.position, new Vector3(moveVector.x, moveVector.y + 1, 0f) * 2, Color.red, 0.1f, true);

        foreach (RaycastHit2D hit2D in raycastHit2D)
        {
            if (hit2D.collider != null && enemyState != GhostEnemyStates.Hide)
            {
                playerPosition = hit2D.collider.transform.position;
                UpdateEnemyState(GhostEnemyStates.Chase);
            }
            else
                playerPosition = Vector2.zero;
        }
    }

    public void ChangeEnemyGhostState(GhostEnemyStates enemyNewStates)
    {

        UpdateEnemyState(enemyNewStates);
    }

    public float ReturnMoveLength()
    {
        return enemyMoveLength;
    }

    public void BackToStartPosition()
    {
        transform.position = enemyStartPosition;
    }
}
