using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class EnemyBatMove : MonoBehaviour
{
    [SerializeField] private float flySpeed = 1f;
    [SerializeField] private float moveUpDownSpeed = 1f;

    [SerializeField] private float FlyDistance = 1f;
    private float pos = 0;

    private Vector3 startPosition;
    [SerializeField] private bool moveRight = false;
    private Vector2 moveVectorHorizontal;
    private bool canMove = true;

    public bool CanMove {get => canMove; set => canMove = value ;}

    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(!canMove)
        return;

        MoveVerical();
        MoveHorizontal();
    }

    private void MoveVerical()
    {
        pos += Time.deltaTime * moveUpDownSpeed;
        transform.position = new Vector3 (transform.position.x, startPosition.y + Mathf.Sin(pos), transform.position.z);
    }

    private void MoveHorizontal()
    {
         if (!moveRight)
        {
            if (transform.position.x <= startPosition.x - FlyDistance)
            {
                moveVectorHorizontal = Vector2.right;
                transform.localScale = new Vector2(1,1);

            }
            if (transform.position.x >= startPosition.x)
            {
                moveVectorHorizontal = Vector2.left;
                transform.localScale = new Vector2(-1,1);
            }
        }
        if (moveRight)
        {
            if (transform.position.x <= startPosition.x)
            {
                moveVectorHorizontal = Vector2.right;
                transform.localScale = new Vector2(1,1);
            }
            if (transform.position.x >= startPosition.x + FlyDistance)
            {
                moveVectorHorizontal = Vector2.left;
                transform.localScale = new Vector2(-1,1);
            }
        }
        transform.Translate(moveVectorHorizontal * flySpeed * Time.deltaTime);
    }
}
