using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpiedrHideFromLight : MonoBehaviour
{
    private LampSetting lampGameObjectSetting;
    private EnemySpiderMove enemySpiderMove;

    private void Start()
    {
        enemySpiderMove = GetComponent<EnemySpiderMove>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Lamp"))
        {
            lampGameObjectSetting = other.gameObject.GetComponent<LampSetting>();
        }
    }
    private void FixedUpdate()
    {
        if (lampGameObjectSetting == null)
            return;
        if (lampGameObjectSetting.ReturnStatusOfLamp())
            enemySpiderMove.EnemySpiderHideStatus(true);
        if (!lampGameObjectSetting.ReturnStatusOfLamp())
            enemySpiderMove.EnemySpiderHideStatus(false);
    }

}
