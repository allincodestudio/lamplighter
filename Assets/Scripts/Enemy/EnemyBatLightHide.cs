using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBatLightHide : MonoBehaviour
{
    // Start is called before the first frame update
    private EnemyBatMove enemyBatMove;
    private Vector3 startPosition;

    private LampSetting? lampSetting = null;

    private GhostEnemyStates ghostEnemyStates;

    private GhostEnemyStates currentState;
    [SerializeField] private float moveUpSpeed = 1f;

    private Animator animator;

    private Vector3 batLocalScaleSet;

    private void Start()
    {
        currentState = GhostEnemyStates.Patrol;
        enemyBatMove = GetComponent<EnemyBatMove>();
        animator = GetComponent<Animator>();

    }

      private void FixedUpdate()
    {
        UpdateBatState(currentState);

        //if (lampSetting == null)
        //    return;

        //if (!lampSetting.ReturnStatusOfLamp())
        //{
        //    lampSetting = null;
        //    LampIsOffBacktoPosition();
        //}
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag.Equals("Lamp"))
        {
            Debug.Log("Lamp Colision");
            lampSetting = other.GetComponent<LampSetting>();
            if (lampSetting.ReturnStatusOfLamp())
            {
                batLocalScaleSet = transform.localScale;
                enemyBatMove.CanMove = false;
                if (currentState == GhostEnemyStates.Patrol)
                    startPosition = transform.position;
                animator.SetBool("Fly", false);
                animator.SetBool("FlyUp", true);
                UpdateBatState(GhostEnemyStates.Hide);
            }
        }

      
    }

    private void OnTriggerEnter2D(Collider2D other) {
          if (other.tag.Equals("Ground"))
        {
            Debug.Log("GroundColision");
            animator.SetBool("FlyUp", false);
            animator.SetBool("Hide", true);
            transform.localScale = new Vector3(1, -1, 1);
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z);
            UpdateBatState(GhostEnemyStates.Wait);

        }
    }

    private void UpdateBatState(GhostEnemyStates newState)
    {
        currentState = newState;

        switch (currentState)
        {
            case GhostEnemyStates.Patrol:
                EnemyBatPatrol();
                break;

            case GhostEnemyStates.Hide:
                EnemyBatHide();
                break;

            case GhostEnemyStates.Wait:
                EnemyBatWait();
                break;

            case GhostEnemyStates.MoveBack:
                EnemyBatBackFromHide();
                break;

            default:
                EnemyBatPatrol();
                break;
        }
    }

     private void EnemyBatPatrol()
    {
        enemyBatMove.CanMove = true;
    }

     private void EnemyBatHide()
    {
        if (!lampSetting.ReturnStatusOfLamp())
        {
            FlyDownSettings();
            return;
        }
        //play fly up animation
        transform.Translate(Vector2.up * moveUpSpeed * Time.deltaTime);

    }


    private void EnemyBatWait()
    {


        // play idle animation
        //check if lamp is still on 
        // swap sprite by x axis 
        // on exit swap back 

        if (lampSetting.ReturnStatusOfLamp())
            return;

        FlyDownSettings();

    }

   

    private void EnemyBatBackFromHide()
    {

        if (lampSetting.ReturnStatusOfLamp())
        {
            UpdateBatState(GhostEnemyStates.Hide);
            animator.SetBool("FlyUp", true);
            animator.SetBool("FlyDown", false);
            return;
        }

        if (transform.position.y > startPosition.y)
        {
            transform.Translate(Vector2.down * moveUpSpeed * Time.deltaTime);
            return;
        }
        else
        {
            UpdateBatState(GhostEnemyStates.Patrol);
            animator.SetBool("Fly", true);
            animator.SetBool("FlyDown", false);
        }
    }

   

     private void FlyDownSettings()
    {
        animator.SetBool("Hide", false);
        animator.SetBool("FlyDown", true);
        animator.SetBool("FlyUp", false);
        transform.localScale = batLocalScaleSet;
        UpdateBatState(GhostEnemyStates.MoveBack);
    }

  

    private void LampIsOffBacktoPosition()
    {

    }
}
