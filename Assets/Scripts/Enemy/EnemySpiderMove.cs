using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class EnemySpiderMove : MonoBehaviour
{
    [SerializeField] private float moveMaxLength;

    [SerializeField] private float[] positionsToHangDown;

    [SerializeField] private float hangLength;

    [SerializeField] private float moveSpeed;

    [SerializeField] private float movingDownSpeed;

    [SerializeField] private bool moveRight;

    private bool moveVerical = false;

    private bool enemyHide = false;

    private Vector2 moveVectorHorizontal = Vector2.zero;

    private Vector2 horizontalStartPosition;

    private Vector2 verticalStartPosition;

    private Vector2 moveVectorVertical;

    private int positionIndexer = 0;

    private EnemySpiderLineRenderer enemySpiderLineRenderer;

    private Animator animator;

    void Start()
    {
        enemySpiderLineRenderer = GetComponent<EnemySpiderLineRenderer>();
        horizontalStartPosition = transform.position;
        animator = GetComponent<Animator>();
        if (!moveRight)
            moveMaxLength = moveMaxLength * -1;
    }

    void Update()
    {
        if (enemyHide)
        {
            EnemySpiderHide();
            return;
        }
        if (!moveVerical)
            EnemySpiderMoveHorizontal();

        if (moveVerical)
            EnemySpiderMoveVertical();
    }

    private void EnemySpiderMoveHorizontal()
    {
        animator.SetBool("moveHorizontal", true);
        
        if (!moveRight)
        {
            if (positionIndexer < positionsToHangDown.Length && (horizontalStartPosition.x - positionsToHangDown[positionIndexer]) - transform.position.x >= 0 && !moveVerical)
            {
                verticalStartPosition = transform.position;
                moveVerical = true;
                animator.SetBool("moveDown", true);
                animator.SetBool("moveVertical", true);
                animator.SetBool("moveHorizontal",false);
                
                return;
            }

            if (transform.position.x <= horizontalStartPosition.x + moveMaxLength)
            {
                moveVectorHorizontal = Vector2.right;
                transform.localScale = new Vector2(1,-1);
            }
            if (transform.position.x >= horizontalStartPosition.x)
            {
                moveVectorHorizontal = Vector2.left;
                transform.localScale = new Vector2(-1,-1);
                positionIndexer = 0;
            }
        }
        if (moveRight)
        {
            if (positionIndexer < positionsToHangDown.Length && horizontalStartPosition.x + positionsToHangDown[positionIndexer] - transform.position.x < Mathf.Epsilon && !moveVerical)
            {
                verticalStartPosition = transform.position;
                moveVerical = true;
                animator.SetBool("moveDown", true);
                animator.SetBool("moveVertical", true);
                animator.SetBool("moveHorizontal",false);
                
                return;
            }

            if (transform.position.x <= horizontalStartPosition.x)
            {
                moveVectorHorizontal = Vector2.right;
                positionIndexer = 0;
                transform.localScale = new Vector2(1,-1);
            }
            if (transform.position.x >= horizontalStartPosition.x + moveMaxLength)
            {
                moveVectorHorizontal = Vector2.left;
                transform.localScale = new Vector2(-1,-1);
            }
        }
        transform.Translate(moveVectorHorizontal * moveSpeed * Time.deltaTime);
    }

    private void EnemySpiderMoveVertical()
    {
        transform.localScale = new Vector2(-1,1);
        if (transform.position.y >= verticalStartPosition.y && moveVectorVertical == Vector2.up)
        {
            positionIndexer++;
            moveVerical = false;
            animator.SetBool("moveVertical", false);
            animator.SetBool("moveHorizontal", true);
            transform.localScale = new Vector2(1,-1);
            moveVectorVertical = Vector2.zero;
            enemySpiderLineRenderer.RenderSpiderLineValues(false, transform.position);

            return;
        }
        if (transform.position.y >= verticalStartPosition.y)
        {
            moveVectorVertical = Vector2.down;
            animator.SetBool("moveDown", true);
        }
        if (transform.position.y <= verticalStartPosition.y - hangLength)
        {
            moveVectorVertical = Vector2.up;
            animator.SetBool("moveDown", false);
        }

        transform.Translate(moveVectorVertical * movingDownSpeed * Time.deltaTime);
        enemySpiderLineRenderer.RenderSpiderLineValues(true, verticalStartPosition);
    }

    private void EnemySpiderHide()
    {
            animator.SetBool("moveHorizontal", false);
            animator.SetBool("moveVertical", false);
        if (transform.position.y >= verticalStartPosition.y)
        {
            positionIndexer = 0;
            moveVerical = false;
            moveVectorVertical = Vector2.zero;
            animator.SetBool("moveHorizontal", true);
            transform.localScale = new Vector2(1,-1);
            enemySpiderLineRenderer.RenderSpiderLineValues(false, transform.position);
        }
        if (moveVerical)
        {
            transform.Translate(Vector2.up * movingDownSpeed * 2 * Time.deltaTime);
            transform.localScale = new Vector2(-1,1);
            animator.SetBool("moveVertical", true);
            animator.SetBool("moveDown", true);
            enemySpiderLineRenderer.RenderSpiderLineValues(true, verticalStartPosition);
            return;
        }
        else
        {
            enemySpiderLineRenderer.RenderSpiderLineValues(false, transform.position);
            animator.SetBool("moveHorizontal", true);
        }
        if (transform.position.x > horizontalStartPosition.x && moveRight)
        {
            transform.Translate(Vector2.left * moveSpeed * 2 * Time.deltaTime);
            transform.localScale = new Vector2(-1,-1);
        }
        if (transform.position.x < horizontalStartPosition.x && !moveRight)
        {
            transform.Translate(Vector2.right * moveSpeed * 2 * Time.deltaTime);
            transform.localScale = new Vector2(1,-1);
        }
        if (transform.position.x <= horizontalStartPosition.x && moveRight)
        {
            moveVectorHorizontal = Vector2.zero;
            transform.position = horizontalStartPosition;
            animator.SetBool("moveHorizontal", false);
            animator.SetBool("moveVertical", false);
        }
        if (transform.position.x >= horizontalStartPosition.x && !moveRight)
        {
            moveVectorHorizontal = Vector2.zero;
            transform.position = horizontalStartPosition;
            animator.SetBool("moveHorizontal", false);
            animator.SetBool("moveVertical", false);
        }
    }

    public void EnemySpiderHideStatus(bool status)
    {
        enemyHide = status;
    }
}
