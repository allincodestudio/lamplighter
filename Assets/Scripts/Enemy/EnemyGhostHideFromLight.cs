using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGhostHideFromLight : MonoBehaviour
{
    private EnemyGhostMove enemyGhostMove;

    private CapsuleCollider2D capsuleCollider2D;

    private Animator animator;

    private RaycastHit2D[] raycastHit2D = new RaycastHit2D[2];

    private bool chechLamp = false;

    private float rayLength;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        enemyGhostMove = GetComponent<EnemyGhostMove>();
        rayLength = enemyGhostMove.ReturnMoveLength();
    }

    private void LateUpdate()
    {


        if (!chechLamp)
            return;

        raycastHit2D[0] = Physics2D.Raycast(transform.position, Vector2.right, rayLength, LayerMask.GetMask("Lamp"));
        raycastHit2D[1] = Physics2D.Raycast(transform.position, Vector2.left, rayLength, LayerMask.GetMask("Lamp"));
        Debug.DrawRay(transform.position, Vector2.right * rayLength, Color.green, 0.1f);
        Debug.DrawRay(transform.position, Vector2.left * rayLength, Color.green, 0.1f);

        LampSetting lampSetting = null;

        foreach (RaycastHit2D hit2D in raycastHit2D)
        {
            if (hit2D.collider != null)
                lampSetting = hit2D.collider.GetComponent<LampSetting>();
        }
        if (lampSetting == null)
        {
            chechLamp = false;
            EnemyApear();
            return;
        }
        if (lampSetting.ReturnStatusOfLamp())
            return;
        else
        {
            chechLamp = false;
            EnemyApear();
        }

    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag.Equals("Lamp") && other.GetComponent<LampSetting>().ReturnStatusOfLamp())
        {
            enemyGhostMove.ChangeEnemyGhostState(GhostEnemyStates.Hide);
            capsuleCollider2D.enabled = false;
            animator.SetBool("Hide", true);
        }
    }

    public void EnemyDisapearAnimatorEvent()
    {
        enemyGhostMove.BackToStartPosition();
        chechLamp = true;
    }

    private void EnemyApear()
    {
        animator.SetBool("Hide", false);
    }

    public void EnemyApearEndedAnimationEvent()
    {
        capsuleCollider2D.enabled = true;
        enemyGhostMove.ChangeEnemyGhostState(GhostEnemyStates.Patrol);

    }
}
