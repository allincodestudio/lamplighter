using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformCheck : MonoBehaviour
{
    [SerializeField] private float _platformCheckRadius = 0.05f;

    [SerializeField] private LayerMask _collisionLayerMask;

    public bool CheckCollision()
    {

        return Physics2D.OverlapCircle(transform.position, _platformCheckRadius, _collisionLayerMask);
    }
}
