using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestResumeButton : MonoBehaviour
{
   public void ResumeGame()
   {
        TestGameManager.TestGameManagerInstance.ResumeGameButton();
   }
}
