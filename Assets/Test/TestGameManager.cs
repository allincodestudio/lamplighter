using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestGameManager : MonoBehaviour
{

    private static TestGameManager testGameManager;

    public static TestGameManager TestGameManagerInstance{
        get {if(testGameManager == null)  testGameManager = new TestGameManager(); return testGameManager;}}

    [SerializeField] private InputReader inputReader;

    [SerializeField] private  GameObject PauseMenu;

    [SerializeField] private Button resumeButton;

    private void Awake() {
              if(testGameManager == null)
        {
            testGameManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        inputReader.Pause += PauseGame;
        inputReader.Resume += ResumeGame;
    }

    private void ResumeGame()
    {
        PauseMenu.SetActive(false);
    }

    private void PauseGame()
    {
        PauseMenu.SetActive(true);
    }

    public void ResumeGameButton()
    {
        PauseMenu.SetActive(false);
        inputReader.SetGamePlay();
    }


    // Update is called once per frame

}
