using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestPlayerMove : MonoBehaviour
{

    [SerializeField] private InputReader inputReader;
    [SerializeField] private int playerMoveSpeed = 5;

    public event Action<bool> PlayerStartJumpEvent;

    private float acceleration = 2;

    private Vector2 _moveVector = Vector2.zero;
    private Vector2 newMovevector;

    private bool _run = false;

    //private bool _jump;

    public Vector2 moveVector { get => _moveVector; }

    public bool run{get => _run;}

    //public bool jumpValue{get => _jump; private set => _jump = value;}


    private void Awake()
    {
        /*
        moveAction = inputActionsAsset.FindActionMap("Player").FindAction("Move");
        inputActionsAsset.FindActionMap("Player").FindAction("MoveVertical").started +=PlayerStartMove;
        inputActionsAsset.FindActionMap("Player").FindAction("MoveVertical").canceled +=PlayerStopMove;
        inputActionsAsset.FindActionMap("Player").FindAction("Jump").started += JumpOn;
        inputActionsAsset.FindActionMap("Player").FindAction("Jump").canceled += JumpOFF;
        */
        inputReader.MoveEvent += PlayerMoveEvent;
        inputReader.JumpEvent += PlayerJumpEvent;
        inputReader.RunEvent += PlayerRunEvent;
    }

    private void PlayerRunEvent(bool run)
    {
        _run = run;
    }

    /*

        private void PlayerStopMove(InputAction.CallbackContext context)
        {
            //holdDownButton = false;

        }

        private void PlayerStartMove(InputAction.CallbackContext context)
        {
            //Debug.Log(context.ReadValue<float>());
            // moveVector = new Vector2(0 + context.ReadValue<float>(),0);
            // holdDownButton = true;
        }

        private void JumpOFF(InputAction.CallbackContext context)
        {
            //holdDownButton = false;
            //rigidBody2D.AddForce(Vector2.up * playerMoveSpeed * -1000);
            //Debug.Log(Time.fixedTime);
        }

        private void JumpOn(InputAction.CallbackContext context)
        {
            //rigidBody2D.AddForce(Vector2.up * playerMoveSpeed * 1000);
            //holdDownButton = true;
            //Debug.Log(Time.fixedTime);
        }
    */

    private void PlayerJumpEvent(bool jump)
    {
        //jumpValue = true;
        //Debug.Log("Jump");
        PlayerStartJumpEvent?.Invoke(jump);

        //Debug.Log(PlayerStartJumpEvent.ToString());
    }

    private void PlayerMoveEvent(Vector2 vector)
    {
        _moveVector = vector;
    }

    private void OnDisable()
    {
        inputReader.MoveEvent -= PlayerMoveEvent;
        inputReader.JumpEvent -= PlayerJumpEvent;
        inputReader.RunEvent -= PlayerRunEvent;
    }

    private void PlayerJump()
    {
        //if (jump)
        //rigidBody2D.AddForce(Vector2.up * 20f);
        //jump = false;
    }

    public void PlayerMove()
    {

        //holdDownButton = true;

        /*
        if (moveVector == Vector2.zero)
            return ;

        if (moveVector != Vector2.zero)
        {
            newMovevector = moveVector;

            acceleration += 2f * Time.deltaTime;
            if (acceleration > playerMoveSpeed)
                acceleration = playerMoveSpeed;
            //rigidBody2D.velocity = newMovevector * acceleration;
            //holdDownButton = true;
        }

        if (moveVector == Vector2.zero && acceleration > 2)
        {
            acceleration -= 20f * Time.deltaTime;
            //acceleration = 2;
            //rigidBody2D.velocity = newMovevector * acceleration;

        }
        else
        {
            acceleration = 2;
        }

        */


        //holdDownButton = false;
        /*
                    if(moveVector.x != 0)
                    {
                        animator.SetBool("PlayerWalk", true);
                        acceleration += 2f * Time.deltaTime;
                        if(acceleration > playerMoveSpeed)
                        acceleration = playerMoveSpeed ;
                        rigidBody2D.velocity = newMovevector * acceleration ;
                        animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));

                        //rigidBody2D.AddForce(Vector2.up * playerMoveSpeed * 10);
                    }
                    
        else if (!holdDownButton && acceleration > 2)
        {
            acceleration -= 20f * Time.deltaTime;
            //acceleration = 2;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));
        }
        else
        {
            animator.SetBool("PlayerWalk", false);
            acceleration = 2;
        }

        */
    }

    /*
        private void OnEnable()
        {
            inputActionsAsset.FindActionMap("Player").Enable();
        }
        private void OnDisable()
        {
            inputActionsAsset.FindActionMap("Player").Disable();
        }

        */

    private void FixedUpdate()
    {
        //rigidBody2D.velocity = moveImput * playerMoveSpeed;

    }

    private void Update()
    {
        //PlayerMove();
        //PlayerJump();

        /*
        if (moveVector.x > 0)
            spriteRenderer.flipX = true;
        else if (moveVector.x < 0)
            spriteRenderer.flipX = false;
        if (moveVector != Vector2.zero)
        {
            newMovevector = moveVector;
            holdDownButton = true;
        }
        else
            holdDownButton = false;

        if (holdDownButton)
        {
            animator.SetBool("PlayerWalk", true);
            acceleration += 2f * Time.deltaTime;
            if (acceleration > playerMoveSpeed)
                acceleration = playerMoveSpeed;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));

            //rigidBody2D.AddForce(Vector2.up * playerMoveSpeed * 10);
        }
        else if (!holdDownButton && acceleration > 2)
        {
            acceleration -= 20f * Time.deltaTime;
            //acceleration = 2;
            rigidBody2D.velocity = newMovevector * acceleration;
            animator.SetFloat("PlayerWalkRun", Mathf.Abs(rigidBody2D.velocity.x));
        }
        else
        {
            animator.SetBool("PlayerWalk", false);
            acceleration = 2;
        }

        */

        //if(moveAction.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
        //holdDownButton = true;
        /*
                else
                {
                    holdDownButton = false;
                    acceleration = 0;
                }

                if(holdDownButton)
                {
                    if(acceleration < 5)
                    {
                        acceleration += Time.deltaTime;
                    }
                    */
        //var move = moveAction.ReadValue<Vector2>();
        //rigidBody2D.velocity = move * playerMoveSpeed;



        /*
        if(keyboard == null)
        return;

        if(keyboard.aKey.isPressed)
        {moveImput.x = -1;}
        if(keyboard.dKey.isPressed)
        {moveImput.x = 1;}
        if(keyboard.wKey.isPressed)
        rigidBody2D.AddForce(Vector2.up * playerMoveSpeed);
        */
    }
    /*
        private void OnMove(InputValue inputValue)
        {
            //moveImput = inputValue.Get<Vector2>();
            //Debug.Log("PLayer move speed " + playerMoveSpeed*Time.deltaTime);

        }
        */

}
