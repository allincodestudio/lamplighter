using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[CreateAssetMenu(fileName = "InputReader", menuName = "InputReader/InputReader", order = 0)]
public class InputReader : ScriptableObject, PlayerInput.IPlayerActions, PlayerInput.IUIActions
{

    private PlayerInput gameInput;

    public event Action<Vector2> MoveEvent;
    public event Action<bool> JumpEvent;

    public event Action<bool> RunEvent;
    public event Action Pause;
    public event Action Resume;
    public event Action OnActionKeyEvent;

    private void OnEnable()
    {
        if (gameInput == null)
        {
            gameInput = new PlayerInput();

        gameInput.Player.SetCallbacks(this);
        gameInput.UI.SetCallbacks(this);
        }

        SetGamePlay();
    }

    private void OnDisable() {
        gameInput.Player.Disable();
        gameInput.UI.Disable();
    }

    public void SetGamePlay()
    {
        gameInput.UI.Disable();
        gameInput.Player.Enable();
    }

    public void SetUI()
    {
        gameInput.Player.Disable();
        gameInput.UI.Enable();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if(context.performed)
        JumpEvent?.Invoke(true);
        if(context.canceled)
        JumpEvent?.Invoke(false);
    }


    public void OnMove(InputAction.CallbackContext context)
    {
        MoveEvent?.Invoke(context.ReadValue<Vector2>());
    }

    public void OnRun(InputAction.CallbackContext context)
    {
        if(context.performed)
        RunEvent?.Invoke(true);
        if(context.canceled)
        RunEvent?.Invoke(false);
        
    }

  

    public void OnPause(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
        Pause?.Invoke();
        SetUI();
        }
    }

   

    public void OnActionKey(InputAction.CallbackContext context)
    {
        if(context.performed)
        OnActionKeyEvent?.Invoke();
    }

    public void OnResume(InputAction.CallbackContext context)
    {
        if(context.performed)
        {
        Resume?.Invoke();
        SetGamePlay();
        }
    }

}

